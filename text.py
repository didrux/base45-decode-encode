C:\Users\misha\AppData\Local\Programs\Python\Python39\python.exe C:/Users/misha/Desktop/covid/main.py
QR Code Issuer : AT
QR Code Expiry : 2021-06-23 14:29:57
QR Code Generated : 2021-06-21 14:29:57
 Vaccination Group
   Dose Number : 1
   Marketing Authorization Holder : ORG-100030215
   vaccine or prophylaxis : 1119349007
   ISO8601 complete date: Date of Vaccination : 2021-02-18
   Country of Vaccination : AT
   Unique Certificate Identifier: UVCI : URN:UVCI:01:AT:10807843F94AEE0EE5093FBC254BD813#B
   vaccine medicinal product : EU/1/20/1528
   Certificate Issuer : Ministry of Health, Austria
   Total Series of Doses : 2
   disease or agent targeted : 840539006
 Surname(s), forename(s)
   Standardised surname : MUSTERFRAU<GOESSINGER
   Surname : Musterfrau-Gößinger
   Standardised forename : GABRIELE
   Forename : Gabriele
 Schema version : 1.2.1
 Date of birth : 1998-02-26

Process finished with exit code 0
